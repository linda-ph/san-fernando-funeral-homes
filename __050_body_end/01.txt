
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="footer-w3layouts">
				<div class="col-md-3 footer-agileits">
					<h3>Partners</h3>
					<ul>
						<li><a href="https://www.doh.gov.ph/">DOH</a> </li>
						<li><a href="http://nbi.gov.ph/">NBI</a> </li>
						<li>Philippine Embalmers & Morticians Association</li>
						<li>Philippine Mortuary Association</li>
						<li><a href="http://www.cclpi.com.ph">Cosmopolitan CLIMBS Life Plan</a> </li>
						<li><a href="https://www.cosmopolitanfuneral.com.ph/">Cosmopolitan Memorial Chapels</a> </li>
						<li><a href="https://www.facebook.com/pcas.cebu">Pacific Center for Advanced Studies</a> </li>
					</ul>
				</div>
				<div class="col-md-3 footer-wthree">
					<h3>Products</h3>
					<ul>						
						<li><a href="caskets.html">Caskets</a></li>
						<li><a href="urns.html">Urns</a></li> 
					</ul>
				</div>
				<div class="col-md-3 footer-w3-agileits">
					<h3>Facilities</h3>
					<ul>						
						<li><a href="chapels.html">Chapels</a></li>
						<li><a href="crematorium.html">Crematorium</a></li>
						<li><a href="floral_garden.html">Floral Garden</a></li>
						<li><a href="columbarium.html">Columbarium</a></li>
					</ul>
				</div>
				<div class="col-md-3 footer-agileits-w3layouts">
					<h3>Navigation</h3>
					<ul>
						<li><a href="index.html">Home</a></li>
						<li><a href="about.html">About</a></li>
						<li><a href="branches.html">Branches</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="footer-w3-agile">
				<div class="col-md-6 w3l-footer-top">
					<h3>Newsletter</h3>
					<form action="#" method="post" class="newsletter">
						<input class="email" type="email" placeholder="Your email..." required="">
						<input type="submit" class="submit"  value="">
					</form>
					<div class="footer-agile">
						<div class="col-md-6 footer-w3-1">
							<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
							<p> Lipa Floral Garden, </p>
							<p> Brgy. Marawoy, Lipa City, </p>
							<p> Batangas 4217, Philippines</p>
						</div>
						<div class="col-md-6 footer-w3l-1">
							<span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
							<p> <a href="tel:+63437573255">(+6343) 757-3255</a></p>  
                            				<p> <a href="tel:+63437741844">(+6343) 774-1844</a></p>  
                           				<p> <a href="tel:+6324118292">(+632) 411-8292</a></p>
						</div>
							<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6 w3ls-social-icons">
					<h3>Follow Us</h3>
					<a class="facebook" href="https://www.facebook.com/sanfernandofunerals/"><i class="fa fa-facebook"></i></a>
					<a class="twitter" href="https://twitter.com/sanfernandoteam"><i class="fa fa-twitter"></i></a>
					<a class="linkedin" href="https://www.linkedin.com/company/san-fernando-funeral-homes-crematory-inc/"><i class="fa fa-linkedin"></i></a>
					<a class="vimeo" href="https://vimeo.com/sanfernandofunerals"><i class="fa fa-vimeo"></i></a>
					<a class="gitlab" href="#"><i class="fa fa-gitlab"></i></a>
					<a class="yelp" href="https://www.yelp.com/biz/san-fernando-funeral-homes-and-crematory-lipa"><i class="fa fa-yelp"></i></a>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="copy-right-agile">
				<p>© 2018 San Fernando Funeral Homes & Crematory, Inc. All rights reserved. | Design: <a href="http://w3layouts.com/">W3layouts</a> | Development: <a href="it_unit.html">Team San Fernando</a></p>
			</div>
		</div>
	</div>
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
