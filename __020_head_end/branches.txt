
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="funeral home, funeral parlor, death care, funeral services, crematorium, cremation, embalming, mortuary, mausoleum, Lipa City, Quezon City" />
<meta name="description" content="San Fernando Funeral Homes & Crematory, Inc. is a SEC-registered, 100% Filipino-owned, and family-ran enterprise that specializes in postmortem care and services." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- pop-up-script -->
<link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
<!-- //pop-up-script -->
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- 2018-03-05 edited -->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.js"></script>
<![endif]-->
<!-- //HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- for maps -->
<link href="css/map.css" rel="stylesheet" type="text/css" media="all" />
<!-- //for maps -->
<!-- //2018-03-05 edited -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" media="all" /> 
<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //2018-05-26 -->
<!-- for waze on mobile only -->
<link href="css/waze.css" rel="stylesheet" type="text/css" media="all" />
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- //start-smoth-scrolling -->
<!-- Facebook Developer App -->
<meta property="og:title" content="San Fernando Funeral Homes & Crematory, Inc." />
<meta property="og:type" content="website" />
<meta property="og:description" content="San Fernando Funeral Homes & Crematory, Inc. is a SEC-registered, 100% Filipino-owned, and family-ran enterprise that specializes in postmortem care and services." />
<meta property="og:url" content="https://www.sanfernandofunerals.com.ph/" />
<meta property="og:image" content="https://i.imgur.com/p4ysrei.png" />
<meta property="og:image:secure_url" content="https://i.imgur.com/p4ysrei.png" />
<meta property="fb:app_id" content="199026854179118" />
<!-- //Facebook Developer App -->
</head>
