
							<li class="menu__item menu__item--current"><a href="index.html" class="menu__link">Home</a></li>
							<li class="menu__item"><a href="about.html" class="menu__link">About</a></li>
							<li class="dropdown menu__item">
								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Products <b class="caret"></b></a>
								<ul class="dropdown-menu agile_short_dropdown">
									<li><a href="caskets.html">Caskets</a></li>
									<li><a href="urns.html">Urns</a></li>
								</ul>
							</li>
							<li class="dropdown menu__item">
								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown">Facilities <b class="caret"></b></a>
								<ul class="dropdown-menu agile_short_dropdown">
									<li><a href="special_care_room.html">Special Care</a></li>
									<li><a href="selection_room.html">Selection</a></li>
									<li><a href="chapels.html">Chapels</a></li>
									<li><a href="crematorium.html">Crematorium</a></li>
									<li><a href="floral_garden.html">Floral Garden</a></li>
									<li><a href="columbarium.html">Columbarium</a></li>
									<li><a href="fleet.html">Fleet</a></li>
									<li><a href="fabrication_plant.html">Fabrication</a></li>
								</ul>
							</li>
							<li class="menu__item"><a href="branches.html" class="menu__link">Branches</a></li>
							<li class="menu__item"><a href="contact.html" class="menu__link">Contact</a></li>