
<!-- Floral Garden -->
	<div class="portfolio">
		<div class="container">
			<h2 class="w3ls_head">Floral Garden</h2>
			<p class="w3layouts_para">The Lipa Floral Garden is located at the very heart of the city for easy accessibility. We offer lot area for traditional lawn burial, memorial, family estate, and mausoleum too.</p>
			<div class="portfolio-agile">
			<div class="main">
                <div class="view view-seventh">
					<a href="images/floral_garden/01.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/01-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/02.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/02-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/03.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/03-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/04.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/04-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/05.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/05-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/06.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/06-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/07.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/07-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>
                <div class="view view-seventh">
					<a href="images/floral_garden/08.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="images/floral_garden/08-t.jpg" />
						<div class="mask">
							<h2>Floral Garden</h2>
							<p>Click to magnify.</p>
						</div>
					</a>
                </div>


				<div class="clearfix"></div>
           
			</div>
		</div>
	</div>
<!-- //Floral Garden -->
<!-- pop-up-script -->
<script src="js/jquery.chocolat.js"></script>
<script type="text/javascript" charset="utf-8">
$(function() {
	$('.view-seventh a').Chocolat();
});
</script>
<!-- //pop-up-script -->